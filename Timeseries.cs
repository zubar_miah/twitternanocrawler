﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using TweetSharp;

namespace TwitterNanoCrawlerACOne
{
    public partial class Timeseries : Form
    {
        Chart chart1;
        public Timeseries(List<TwitterStatus> retweetDate)
        {
            InitializeComponent();
            List<DateTime> dates = new List<DateTime>();
            foreach (TwitterStatus twi in retweetDate)
            {
                DateTime dt = new DateTime(twi.CreatedDate.Year, twi.CreatedDate.Month, twi.CreatedDate.Day);
                dates.Add(dt);
            }

            IDictionary<DateTime, int> DateAndCount = new Dictionary<DateTime, int>();

            foreach (DateTime date in dates)
            {
                if (!DateAndCount.ContainsKey(date))
                    DateAndCount.Add(date, 1);
                else
                    DateAndCount[date]++;
            }

            List<DateTime> dlist = new List<DateTime>();
            List<int> ilist = new List<int>();
            for (int i = 0; i < DateAndCount.Count; i++)
            {
                dlist.Add(DateAndCount.ElementAt(i).Key);
                ilist.Add(DateAndCount.ElementAt(i).Value);
            }

            var xvals = dlist.ToArray();
            var yvals = ilist.ToArray();

            chart1 = new Chart();
            chart1.Size = new Size(500,300);

            ChartArea chartArea = new ChartArea();
            chartArea.AxisX.LabelStyle.Format = "dd/MMM\nhh";
            chart1.ChartAreas.Add(chartArea);

            var series = new Series();
            series.Name = "Time";
            series.ChartType = SeriesChartType.FastLine;
            series.XValueType = ChartValueType.DateTime;
            chart1.Series.Add(series);

            chart1.Series["Time"].Points.DataBindXY(xvals, yvals);
            chart1.Show();
            this.Controls.Add(chart1);
            chart1.SaveImage("timeseries.png", ChartImageFormat.Png);
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void chart1_Click_1(object sender, EventArgs e)
        {

        }

        private void Timeseries_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            chart1.Size = this.Size;
        }
    }
}
