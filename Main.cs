﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TweetSharp;
using Tweetinvi;
using TweetinCore.Interfaces;

namespace TwitterNanoCrawlerACOne
{
    public partial class Main : Form
    {
        TwitterService service;
        TwitterNanoUser user;
        List<TwitterStatus> retweetDate;
        List<IUser> outdegree;
        List<IUser> sameList;
        List<IUser> indegree;

        public Main(List<List<IUser>> information)
        {
            InitializeComponent();
            user = MakePrivateConnection.getUser();
            TwitterService service = MakePrivateConnection.getConnection();
            GetUserProfileOptions optiona = new GetUserProfileOptions();
            TwitterUser person = service.GetUserProfile(optiona);
            username.Text = user.getUsername();
            userlocation.Text = user.getLocation();
            followers.Text = user.getFollowerCount().ToString();
            pictureBox1.Load(user.getProfilePicture());
            friends.Text = person.FriendsCount.ToString();
            List<IUser> followersList = information.ElementAt(0);
            List<IUser> friendsList = information.ElementAt(1);
            sameList = information.ElementAt(2);
            
            ListTweetsMentioningMeOptions options = new ListTweetsMentioningMeOptions();
            IEnumerable<TwitterStatus> users = service.ListTweetsMentioningMe(options);
            
            richTextBox1.AppendText("Tweets that mention me");
            richTextBox1.AppendText(Environment.NewLine);
            foreach (TwitterStatus status in users)
            {
                richTextBox1.AppendText(status.User.Name + "  Number of times retweeted " + status.RetweetCount);
                richTextBox1.AppendText(Environment.NewLine);
            }
            richTextBox1.AppendText(Environment.NewLine);

            ListRetweetsOfMyTweetsOptions retweetoptions = new ListRetweetsOfMyTweetsOptions();

            IEnumerable<TwitterStatus> users2 = service.ListRetweetsOfMyTweets(retweetoptions);

             foreach (TwitterStatus status in users2)
             {
                 richTextBox1.AppendText(status.Text + "  Number of times retweeted " + status.RetweetCount);
                 richTextBox1.AppendText(Environment.NewLine);
             }

             richTextBox1.AppendText(Environment.NewLine);
             richTextBox1.AppendText("List of indegree connections");
             richTextBox1.AppendText(Environment.NewLine);

             foreach (IUser iu in sameList)
             {
                 richTextBox1.AppendText(iu.Name + " : " + iu.FollowersCount);
                 richTextBox1.AppendText(Environment.NewLine);
             }

             outdegree = new List<IUser>();

             foreach (IUser iu2 in friendsList)
             {
                 if (sameList.Contains(iu2))
                 {
                 }
                 else
                 {
                     outdegree.Add(iu2);
                 }
             }

             foreach (IUser iu3 in followersList)
             {
                 if (sameList.Contains(iu3))
                 {
                 }
                 else
                 {
                     outdegree.Add(iu3);
                 }
             }

             richTextBox1.AppendText(Environment.NewLine);
             richTextBox1.AppendText("List of outdegree connections");
             richTextBox1.AppendText(Environment.NewLine);

             foreach (IUser iu4 in outdegree)
             {
                 richTextBox1.AppendText(iu4.Name + " : " + iu4.FollowersCount);
                 richTextBox1.AppendText(Environment.NewLine);
             }

             ListTweetsOnUserTimelineOptions loptions = new ListTweetsOnUserTimelineOptions();
             
             loptions.Count = person.StatusesCount;
             loptions.IncludeRts = false;
             IAsyncResult res = service.BeginListTweetsOnUserTimeline(loptions);
             IEnumerable<TwitterStatus> listTweets = service.EndListTweetsOnUserTimeline(res);
             retweetDate = new List<TwitterStatus>();
             richTextBox1.AppendText(Environment.NewLine);
             richTextBox1.AppendText("Amount of tweets from last " + listTweets.ToList().Count + " tweets that where retweeted");
             richTextBox1.AppendText(Environment.NewLine);
             int amount = 0;
             int retweeted = 0;
             foreach (TwitterStatus temp in listTweets)
             {
                 amount++;
                 if (temp.RetweetCount > 0)
                 {
                     retweetDate.Add(temp);
                     retweeted++;
                 }
             }

             richTextBox1.AppendText(amount + " : " + retweeted);
             richTextBox1.AppendText(Environment.NewLine);

        }

        private void Main_Load(object sender, EventArgs e)
        {

        }

        private void username_Click(object sender, EventArgs e)
        {

        }


        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

       
        private void button1_Click(object sender, EventArgs e)
        {
           
        }

        private void friends_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {


        }

        private void timeSeriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var time = new Timeseries(retweetDate);
            time.Show();
        }

        private void outdegreeChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var follow = new OutdegreeChart(outdegree);
            follow.Show();

        }

        private void indegreeChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var follow = new IndegreeChart(sameList);
            follow.Show();
        }

        private void ratioChartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ratiochart = new RatioChart(sameList.Count, outdegree.Count);
            ratiochart.Show();
        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

        }

 
    }
}
