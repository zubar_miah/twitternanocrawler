﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterNanoCrawlerACOne
{
    class TwitterNanoUser
    {
        String Username;
        int FollowerCount;
        long Id;
        String Description;
        String ProfileBackgroundImageUrl;
        String location;
        String ProfilePicture;
        String TweetLocation;

        public TwitterNanoUser(String Username,
        int FollowerCount,
        long Id,
        String Description,
        String ProfilePicture,
        String ProfileBackgroundImageUrl,
        String location)
        {
            this.Username = Username;
            this.FollowerCount = FollowerCount;
            this.Id = Id;
            this.Description = Description;
            this.ProfilePicture = ProfilePicture;
            this.ProfileBackgroundImageUrl = ProfileBackgroundImageUrl;
            this.location = location;
        }

        public TwitterNanoUser(String Username,
        int FollowerCount,
        long Id,
        String Description,
        String ProfilePicture,
        String ProfileBackgroundImageUrl,
        String location,
        String TweetLocation)
        {
            this.Username = Username;
            this.FollowerCount = FollowerCount;
            this.Id = Id;
            this.Description = Description;
            this.ProfilePicture = ProfilePicture;
            this.ProfileBackgroundImageUrl = ProfileBackgroundImageUrl;
            this.location = location;
            this.TweetLocation = TweetLocation;
        }

        public String getUsername()
        {
            return Username;
        }

        public int getFollowerCount()
        {
            return FollowerCount;
        }

        public long getId()
        {
            return Id;
        }

        public String getDescription()
        {
            return Description;
        }

        public String getProfileBackgroundImageUrl()
        {
            return ProfileBackgroundImageUrl;
        }

        public String getLocation()
        {
            return location;
        }

        public String getProfilePicture()
        {
            return ProfilePicture;
        }

        public String getTweetLocation()
        {
            return TweetLocation;
        }

    }
}
