﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading;
using oAuthConnection;
using Streaminvi;
using TweetinCore.Enum;
using TweetinCore.Events;
using TweetinCore.Interfaces;
using TweetinCore.Interfaces.oAuth;
using TweetinCore.Interfaces.StreamInvi;
using TweetinCore.Interfaces.TwitterToken;
using Tweetinvi;
using Tweetinvi.Model;
using TwitterToken;
using UILibrary;
using System.Windows;
using Timer = System.Timers.Timer;

namespace TwitterNanoCrawlerACOne
{
    class MakePublicConnection
    {
        static MakePublicConnection instance = null;
        static String url;
        static String Captcha = "";
        static IToken token;
        public MakePublicConnection()
        {
            token = new Token(
            ConfigurationManager.AppSettings["token_AccessToken"],
            ConfigurationManager.AppSettings["token_AccessTokenSecret"],
            ConfigurationManager.AppSettings["token_ConsumerKey"],
            ConfigurationManager.AppSettings["token_ConsumerSecret"]);

            TokenSingleton.Token = token;

            var t = GenerateTokenFromConsole(token);
            if (t != null)
            {
                token = t;
            }


            CreateUser(token, MakePrivateConnection.getUser().getUsername());

        }

        public static IToken getToken()
        {
            return token;
        }

        public static MakePublicConnection Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MakePublicConnection();
                }
                return instance;
            }
        }

        public static void CreateUser(IToken token, string screenName = null)
        {
            IUser user = new User(screenName, token);
        }

        public static int GetCaptchaFromConsole(string validationUrl)
        {
            url = validationUrl;
            string validationKey = getCapthaCode();

            int result;
            if (Int32.TryParse(validationKey, out result))
            {
                return result;
            }

            return -1;
        }

        public static void setCaptchaCode(string CaptchaA)
        {
            Captcha = CaptchaA;
        }

        public static String getCapthaCode()
        {
            while (true)
            {
                if (Captcha.Equals(""))
                {

                }
                else
                {
                    return Captcha;
                }
            }
        }


        public static String getURL()
        {
            return url;
        }

        public static IToken GenerateTokenFromConsole(IToken consumerToken)
        {
            return GenerateToken(consumerToken, GetCaptchaFromConsole);
        }

        public static IToken GenerateToken(IToken consumerToken, RetrieveCaptchaDelegate getCaptchaDelegate)
        {
            ITokenCreator creator = new TokenCreator(consumerToken.ConsumerKey,
                                                     consumerToken.ConsumerSecret);
            IToken newToken = creator.CreateToken(getCaptchaDelegate);

            if (newToken != null)
            {
                ITokenUser loggedUser = new TokenUser(newToken);
                return newToken;
            }
            return null;
        }

    
        public List<List<IUser>> getFollowerList(String username)
        {
            User temp = new User(username, token);

            List<ITweet> tweets = temp.TweetsRetweetedByFollowers;
            

            List<IUser> list = temp.Followers;
            List<IUser> list2 = temp.Friends;
            Console.WriteLine("_______________");
            Console.WriteLine(list.Count);
            Console.WriteLine(list2.Count);
            List<IUser> comp = new List<IUser>();
            foreach (IUser e in list)
            {
                if (list2.Contains(e))
                {
                    Console.WriteLine(e.ScreenName);
                    comp.Add(e);
                }
            }
            List<IUser> indegree = new List<IUser>();
            foreach (IUser ind in list)
            {
                if (!list2.Contains(ind))
                {
                    indegree.Add(ind);
                }
            }

            List<IUser> outdegree = new List<IUser>();
            foreach (IUser outd in list2)
            {
                if (!list.Contains(outd))
                {
                    indegree.Add(outd);
                }
            }

            List<List<IUser>> userslist = new List<List<IUser>>();
            userslist.Add(list);
            userslist.Add(list2);
            userslist.Add(comp);

            return userslist;
        }

        public List<IUser> getFriendList(String username)
        {
            User temp = new User(username, token);
            List<IUser> list = temp.Friends;
            return list;
        }

    }
   }

