﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace TwitterNanoCrawlerACOne
{
    public partial class RatioChart : Form
    {
        public RatioChart(int indegree, int outdegree)
        {
            InitializeComponent();
            int[] yValues = { indegree, outdegree };
            String[] xValues = { "Indegree", "Outdegree" };
            chart1.Series[0].Points.DataBindXY(xValues, yValues);
            chart1.SaveImage("ratio.png", ChartImageFormat.Png);
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void RatioChart_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            chart1.Size = this.Size;
        }
    }
}
