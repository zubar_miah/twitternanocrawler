﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TweetSharp;
using System.Threading;
using TwitterToken;
using Tweetinvi;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Threading;
using oAuthConnection;
using Streaminvi;
using TweetinCore.Enum;
using TweetinCore.Events;
using TweetinCore.Interfaces;
using TweetinCore.Interfaces.oAuth;
using TweetinCore.Interfaces.StreamInvi;
using TweetinCore.Interfaces.TwitterToken;
using Tweetinvi;
using Tweetinvi.Model;
using TwitterToken;
using UILibrary;
using System.Windows;

namespace TwitterNanoCrawlerACOne
{
    public partial class TokenMaker : Form
    {
        private Main mainmenu;
        MakePrivateConnection connPri;
        MakePublicConnection connPub;
        Thread connectionThread;
        Boolean status = false;
        List<List<IUser>> userslist;
        public TokenMaker()
        {
            InitializeComponent();
            connPri = MakePrivateConnection.Instance;
            String url = MakePrivateConnection.getURL().OriginalString;
            System.Diagnostics.Process.Start(url);
        }

        private void label1_Click(object sender, EventArgs e)
        {
           
        }

        static void getInstance()
        {
            MakePublicConnection connPub = MakePublicConnection.Instance;
        }

        private void TokenMaker_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!status)
            {
                status = makePrivateConenction();
                if (status)
                {
                    connectionThread = new Thread(getInstance);
                    connectionThread.Start();
                    System.Threading.Thread.Sleep(2000);
                    System.Diagnostics.Process.Start(MakePublicConnection.getURL());
                }
            }
            else
            {
                makePublicConnection();
            }
        }

        public Boolean makePrivateConenction()
        {
            try
            {
                MakePrivateConnection.enterPin(textBox1.Text.ToString());
                TwitterService service = MakePrivateConnection.getConnection();
                GetUserProfileOptions option = new GetUserProfileOptions();
                ListTweetsOnUserTimelineOptions tweetOption = new ListTweetsOnUserTimelineOptions();
                TwitterUser person = service.GetUserProfile(option);

                ListTweetsOnUserTimelineOptions optionTweet = new ListTweetsOnUserTimelineOptions();
                optionTweet.ScreenName = person.ScreenName;
                String location = "";
                var statuses = service.ListTweetsOnUserTimeline(optionTweet);

                if ((bool)person.IsGeoEnabled)
                {
                    foreach (var status in statuses)
                    {
                        Console.WriteLine(status.Text);
                        try
                        {
                            location = status.Place.FullName;
                            if (location == "")
                            {
                            }
                            else
                            {
                                break;
                            }

                        }
                        catch (Exception f)
                        {
                            Console.WriteLine("No Location");
                        }
                    }
                }

                if (location.Equals(""))
                {
                    location = "London";
                }

                TwitterNanoUser user = new TwitterNanoUser(person.ScreenName, person.FollowersCount, person.Id, person.Description, person.ProfileImageUrl, person.ProfileBackgroundImageUrl, person.Location, location);
                MakePrivateConnection.setUser(user);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }


        }

        public void makePublicConnection()
        {
            //MakePublicConnection p = MakePublicConnection.Instance;
            MakePublicConnection.setCaptchaCode(textBox1.Text.ToString());
            connectionThread.Abort();
            userslist = MakePublicConnection.Instance.getFollowerList(MakePrivateConnection.getUser().getUsername());
            var mm = new Main(userslist);
            mm.Show();
            this.Hide();
        }

    }
}
