﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetSharp;
using System.Diagnostics;
using System.Collections;

namespace TwitterNanoCrawlerACOne
{
    class MakePrivateConnection
    {

        static MakePrivateConnection instance = null;
        static TwitterService service;
        static OAuthRequestToken requestToken;
        static TwitterNanoUser user;
        static String Token;
        static String TokenSecret;
        static IEnumerable<TwitterStatus> tweets;
        static Uri url;
        MakePrivateConnection()
        {
            service = new TwitterService("", "");
            requestToken = service.GetRequestToken();
            url = service.GetAuthorizationUri(requestToken);
        }

        public static MakePrivateConnection Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MakePrivateConnection();
                }
                return instance;
            }
        }

        public static TwitterService getConnection()
        {
            return service;
        }

        public static OAuthRequestToken getToken()
        {
            return requestToken;
        }

        public static void enterPin(String pin)
        {
            String verifier = pin;
            OAuthAccessToken access = service.GetAccessToken(getToken(), verifier);
            service.AuthenticateWith(access.Token, access.TokenSecret);
            Token = access.Token;
            TokenSecret = access.TokenSecret;
        }

        public static void setUser(TwitterNanoUser usera)
        {
            user = usera;
        }

        public static TwitterNanoUser getUser()
        {
            return user;
        }

        public static String getAuToken()
        {
            return Token;
        }

        public static String getTokenSecret()
        {
            return TokenSecret;
        }

        public static List<TwitterUser> getUsernamesPri(String username)
        {
            List<TwitterUser> listofFollowers = new List<TwitterUser>();
            ListFollowersOptions options = new ListFollowersOptions();
            options.ScreenName = username;
            //options.ScreenName = "Sammy___D";

            TwitterCursorList<TwitterUser> followers = service.ListFollowers(options);

            if (followers == null)
            {

            }
            else
            {
                while (followers.NextCursor != null)
                {
                    if (followers == null)
                    {

                    }
                    else
                    {
                        foreach (TwitterUser user in followers)
                        {
                            listofFollowers.Add(user);
                        }
                    }

                    if (followers.NextCursor != null &&
                        followers.NextCursor != 0)
                    {
                        options.Cursor = followers.NextCursor;
                        followers = service.ListFollowers(options);
                    }
                    else
                        break;
                }
            }


            return listofFollowers;
        }

        public static IEnumerable<TwitterStatus> getTweets()
        {
            return tweets;
        }

        public static void setTweets(IEnumerable<TwitterStatus> tweetsA)
        {
            tweets = tweetsA;
        }

        public static Uri getURL()
        {
            return url;
        }

    }
}
