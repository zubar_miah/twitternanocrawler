﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tweetinvi;
using TweetinCore;
using TweetinCore.Interfaces;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace TwitterNanoCrawlerACOne
{
    public partial class IndegreeChart : Form
    {
        public IndegreeChart(List<IUser> list)
        {
            InitializeComponent();
            List<int> yValuesA = new List<int>();
            List<string> xNamesA = new List<string>();

            foreach (var user in list)
            {
                yValuesA.Add((int)user.FollowersCount);
                xNamesA.Add(user.ScreenName);
            }

            foreach (string name in xNamesA)
            {
                Console.WriteLine(name);
            }

            int[] yValues = yValuesA.ToArray();
            string[] xNames = xNamesA.ToArray();
            chart1.Series[0].Points.DataBindXY(xNames, yValues);
            chart1.SaveImage("indegree.png", ChartImageFormat.Png);
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }
    }
}
